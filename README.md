This package contains some usefull open source libraries integrated with CAN/MoveIt (www.canmoveit.com).

#Boost 1.55.0
The library has been shrunk to a lower set of libraries. 
Additionally, the namespace was changed from boost to CMI to prevent "collisions between this specific boost version to be
linked with CAN/MoveIt and the one that you might have on your system.
This package was created using the tool BCP (http://www.boost.org/doc/libs/1_55_0/tools/bcp/doc/html/index.html).
The exat command used was:

```
#!
./bcp --namespace=CMI --namespace-alias asio thread chrono lockfree system circular_buffer context propert_tree algorithm  property_tree/xml_parser.hpp config build /your-output-directory

```

The prefered way to compile the libraries for your system is:
```
#!
./bootstrap.sh
./b2 --prefix=/opt/cmi link=shared runtime-link=shared threading=multi

```
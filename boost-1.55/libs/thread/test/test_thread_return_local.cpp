// Copyright (C) 2009 Anthony Williams
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_USES_MOVE

#include <boost/thread/thread_only.hpp>
#include <boost/test/unit_test.hpp>

void do_nothing(CMI::thread::id* my_id)
{
    *my_id=CMI::this_thread::get_id();
}

CMI::thread make_thread_return_local(CMI::thread::id* the_id)
{
    CMI::thread t(do_nothing,the_id);
    return CMI::move(t);
}

void test_move_from_function_return_local()
{
    CMI::thread::id the_id;
    CMI::thread x=make_thread_return_local(&the_id);
    CMI::thread::id x_id=x.get_id();
    x.join();
    BOOST_CHECK_EQUAL(the_id,x_id);
}

CMI::unit_test::test_suite* init_unit_test_suite(int, char*[])
{
    CMI::unit_test::test_suite* test =
        BOOST_TEST_SUITE("Boost.Threads: thread move test suite");

    test->add(BOOST_TEST_CASE(test_move_from_function_return_local));
    return test;
}



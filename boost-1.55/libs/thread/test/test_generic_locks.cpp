// (C) Copyright 2008 Anthony Williams
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_VERSION 2

#include <boost/test/unit_test.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread_only.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition_variable.hpp>

void test_lock_two_uncontended()
{
    CMI::mutex m1,m2;

    CMI::unique_lock<CMI::mutex> l1(m1,CMI::defer_lock),
        l2(m2,CMI::defer_lock);

    BOOST_CHECK(!l1.owns_lock());
    BOOST_CHECK(!l2.owns_lock());

    CMI::lock(l1,l2);

    BOOST_CHECK(l1.owns_lock());
    BOOST_CHECK(l2.owns_lock());
}

struct wait_data
{
    CMI::mutex m;
    bool flag;
    CMI::condition_variable cond;

    wait_data():
        flag(false)
    {}

    void wait()
    {
        CMI::unique_lock<CMI::mutex> l(m);
        while(!flag)
        {
            cond.wait(l);
        }
    }

    template<typename Duration>
    bool timed_wait(Duration d)
    {
        CMI::system_time const target=CMI::get_system_time()+d;

        CMI::unique_lock<CMI::mutex> l(m);
        while(!flag)
        {
            if(!cond.timed_wait(l,target))
            {
                return flag;
            }
        }
        return true;
    }

    void signal()
    {
        CMI::unique_lock<CMI::mutex> l(m);
        flag=true;
        cond.notify_all();
    }
};


void lock_mutexes_slowly(CMI::mutex* m1,CMI::mutex* m2,wait_data* locked,wait_data* quit)
{
    CMI::lock_guard<CMI::mutex> l1(*m1);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(500));
    CMI::lock_guard<CMI::mutex> l2(*m2);
    locked->signal();
    quit->wait();
}

void lock_pair(CMI::mutex* m1,CMI::mutex* m2)
{
    CMI::lock(*m1,*m2);
    CMI::unique_lock<CMI::mutex> l1(*m1,CMI::adopt_lock),
        l2(*m2,CMI::adopt_lock);
}

void test_lock_two_other_thread_locks_in_order()
{
    CMI::mutex m1,m2;
    wait_data locked;
    wait_data release;

    CMI::thread t(lock_mutexes_slowly,&m1,&m2,&locked,&release);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(10));

    CMI::thread t2(lock_pair,&m1,&m2);
    BOOST_CHECK(locked.timed_wait(CMI::posix_time::seconds(1)));

    release.signal();

    BOOST_CHECK(t2.timed_join(CMI::posix_time::seconds(1)));

    t.join();
}

void test_lock_two_other_thread_locks_in_opposite_order()
{
    CMI::mutex m1,m2;
    wait_data locked;
    wait_data release;

    CMI::thread t(lock_mutexes_slowly,&m1,&m2,&locked,&release);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(10));

    CMI::thread t2(lock_pair,&m2,&m1);
    BOOST_CHECK(locked.timed_wait(CMI::posix_time::seconds(1)));

    release.signal();

    BOOST_CHECK(t2.timed_join(CMI::posix_time::seconds(1)));

    t.join();
}

void test_lock_five_uncontended()
{
    CMI::mutex m1,m2,m3,m4,m5;

    CMI::unique_lock<CMI::mutex> l1(m1,CMI::defer_lock),
        l2(m2,CMI::defer_lock),
        l3(m3,CMI::defer_lock),
        l4(m4,CMI::defer_lock),
        l5(m5,CMI::defer_lock);

    BOOST_CHECK(!l1.owns_lock());
    BOOST_CHECK(!l2.owns_lock());
    BOOST_CHECK(!l3.owns_lock());
    BOOST_CHECK(!l4.owns_lock());
    BOOST_CHECK(!l5.owns_lock());

    CMI::lock(l1,l2,l3,l4,l5);

    BOOST_CHECK(l1.owns_lock());
    BOOST_CHECK(l2.owns_lock());
    BOOST_CHECK(l3.owns_lock());
    BOOST_CHECK(l4.owns_lock());
    BOOST_CHECK(l5.owns_lock());
}

void lock_five_mutexes_slowly(CMI::mutex* m1,CMI::mutex* m2,CMI::mutex* m3,CMI::mutex* m4,CMI::mutex* m5,
                              wait_data* locked,wait_data* quit)
{
    CMI::lock_guard<CMI::mutex> l1(*m1);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(500));
    CMI::lock_guard<CMI::mutex> l2(*m2);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(500));
    CMI::lock_guard<CMI::mutex> l3(*m3);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(500));
    CMI::lock_guard<CMI::mutex> l4(*m4);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(500));
    CMI::lock_guard<CMI::mutex> l5(*m5);
    locked->signal();
    quit->wait();
}

void lock_five(CMI::mutex* m1,CMI::mutex* m2,CMI::mutex* m3,CMI::mutex* m4,CMI::mutex* m5)
{
    CMI::lock(*m1,*m2,*m3,*m4,*m5);
    m1->unlock();
    m2->unlock();
    m3->unlock();
    m4->unlock();
    m5->unlock();
}

void test_lock_five_other_thread_locks_in_order()
{
    CMI::mutex m1,m2,m3,m4,m5;
    wait_data locked;
    wait_data release;

    CMI::thread t(lock_five_mutexes_slowly,&m1,&m2,&m3,&m4,&m5,&locked,&release);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(10));

    CMI::thread t2(lock_five,&m1,&m2,&m3,&m4,&m5);
    BOOST_CHECK(locked.timed_wait(CMI::posix_time::seconds(3)));

    release.signal();

    BOOST_CHECK(t2.timed_join(CMI::posix_time::seconds(3)));

    t.join();
}

void test_lock_five_other_thread_locks_in_different_order()
{
    CMI::mutex m1,m2,m3,m4,m5;
    wait_data locked;
    wait_data release;

    CMI::thread t(lock_five_mutexes_slowly,&m1,&m2,&m3,&m4,&m5,&locked,&release);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(10));

    CMI::thread t2(lock_five,&m5,&m1,&m4,&m2,&m3);
    BOOST_CHECK(locked.timed_wait(CMI::posix_time::seconds(3)));

    release.signal();

    BOOST_CHECK(t2.timed_join(CMI::posix_time::seconds(3)));

    t.join();
}

void lock_n(CMI::mutex* mutexes,unsigned count)
{
    CMI::lock(mutexes,mutexes+count);
    for(unsigned i=0;i<count;++i)
    {
        mutexes[i].unlock();
    }
}


void test_lock_ten_other_thread_locks_in_different_order()
{
    unsigned const num_mutexes=10;

    CMI::mutex mutexes[num_mutexes];
    wait_data locked;
    wait_data release;

    CMI::thread t(lock_five_mutexes_slowly,&mutexes[6],&mutexes[3],&mutexes[8],&mutexes[0],&mutexes[2],&locked,&release);
    CMI::this_thread::sleep(CMI::posix_time::milliseconds(10));

    CMI::thread t2(lock_n,mutexes,num_mutexes);
    BOOST_CHECK(locked.timed_wait(CMI::posix_time::seconds(3)));

    release.signal();

    BOOST_CHECK(t2.timed_join(CMI::posix_time::seconds(3)));

    t.join();
}

struct dummy_mutex
{
    bool is_locked;

    dummy_mutex():
        is_locked(false)
    {}

    void lock()
    {
        is_locked=true;
    }

    bool try_lock()
    {
        if(is_locked)
        {
            return false;
        }
        is_locked=true;
        return true;
    }

    void unlock()
    {
        is_locked=false;
    }
};

namespace CMI {} namespace boost = CMI; namespace CMI
{
    template<>
    struct is_mutex_type<dummy_mutex>
    {
        BOOST_STATIC_CONSTANT(bool, value = true);
    };
}



void test_lock_five_in_range()
{
    unsigned const num_mutexes=5;
    dummy_mutex mutexes[num_mutexes];

    CMI::lock(mutexes,mutexes+num_mutexes);

    for(unsigned i=0;i<num_mutexes;++i)
    {
        BOOST_CHECK(mutexes[i].is_locked);
    }
}

class dummy_iterator:
    public std::iterator<std::forward_iterator_tag,
                         dummy_mutex>
{
private:
    dummy_mutex* p;
public:
    explicit dummy_iterator(dummy_mutex* p_):
        p(p_)
    {}

    bool operator==(dummy_iterator const& other) const
    {
        return p==other.p;
    }

    bool operator!=(dummy_iterator const& other) const
    {
        return p!=other.p;
    }

    bool operator<(dummy_iterator const& other) const
    {
        return p<other.p;
    }

    dummy_mutex& operator*() const
    {
        return *p;
    }

    dummy_mutex* operator->() const
    {
        return p;
    }

    dummy_iterator operator++(int)
    {
        dummy_iterator temp(*this);
        ++p;
        return temp;
    }

    dummy_iterator& operator++()
    {
        ++p;
        return *this;
    }

};


void test_lock_five_in_range_custom_iterator()
{
    unsigned const num_mutexes=5;
    dummy_mutex mutexes[num_mutexes];

    CMI::lock(dummy_iterator(mutexes),dummy_iterator(mutexes+num_mutexes));

    for(unsigned i=0;i<num_mutexes;++i)
    {
        BOOST_CHECK(mutexes[i].is_locked);
    }
}

class dummy_mutex2:
    public dummy_mutex
{};


void test_lock_ten_in_range_inherited_mutex()
{
    unsigned const num_mutexes=10;
    dummy_mutex2 mutexes[num_mutexes];

    CMI::lock(mutexes,mutexes+num_mutexes);

    for(unsigned i=0;i<num_mutexes;++i)
    {
        BOOST_CHECK(mutexes[i].is_locked);
    }
}

void test_try_lock_two_uncontended()
{
    dummy_mutex m1,m2;

    int const res=CMI::try_lock(m1,m2);

    BOOST_CHECK(res==-1);
    BOOST_CHECK(m1.is_locked);
    BOOST_CHECK(m2.is_locked);
}
void test_try_lock_two_first_locked()
{
    dummy_mutex m1,m2;
    m1.lock();

    CMI::unique_lock<dummy_mutex> l1(m1,CMI::defer_lock),
        l2(m2,CMI::defer_lock);

    int const res=CMI::try_lock(l1,l2);

    BOOST_CHECK(res==0);
    BOOST_CHECK(m1.is_locked);
    BOOST_CHECK(!m2.is_locked);
    BOOST_CHECK(!l1.owns_lock());
    BOOST_CHECK(!l2.owns_lock());
}
void test_try_lock_two_second_locked()
{
    dummy_mutex m1,m2;
    m2.lock();

    CMI::unique_lock<dummy_mutex> l1(m1,CMI::defer_lock),
        l2(m2,CMI::defer_lock);

    int const res=CMI::try_lock(l1,l2);

    BOOST_CHECK(res==1);
    BOOST_CHECK(!m1.is_locked);
    BOOST_CHECK(m2.is_locked);
    BOOST_CHECK(!l1.owns_lock());
    BOOST_CHECK(!l2.owns_lock());
}

void test_try_lock_three()
{
    int const num_mutexes=3;

    for(int i=-1;i<num_mutexes;++i)
    {
        dummy_mutex mutexes[num_mutexes];

        if(i>=0)
        {
            mutexes[i].lock();
        }
        CMI::unique_lock<dummy_mutex> l1(mutexes[0],CMI::defer_lock),
            l2(mutexes[1],CMI::defer_lock),
            l3(mutexes[2],CMI::defer_lock);

        int const res=CMI::try_lock(l1,l2,l3);

        BOOST_CHECK(res==i);
        for(int j=0;j<num_mutexes;++j)
        {
            if((i==j) || (i==-1))
            {
                BOOST_CHECK(mutexes[j].is_locked);
            }
            else
            {
                BOOST_CHECK(!mutexes[j].is_locked);
            }
        }
        if(i==-1)
        {
            BOOST_CHECK(l1.owns_lock());
            BOOST_CHECK(l2.owns_lock());
            BOOST_CHECK(l3.owns_lock());
        }
        else
        {
            BOOST_CHECK(!l1.owns_lock());
            BOOST_CHECK(!l2.owns_lock());
            BOOST_CHECK(!l3.owns_lock());
        }
    }
}

void test_try_lock_four()
{
    int const num_mutexes=4;

    for(int i=-1;i<num_mutexes;++i)
    {
        dummy_mutex mutexes[num_mutexes];

        if(i>=0)
        {
            mutexes[i].lock();
        }
        CMI::unique_lock<dummy_mutex> l1(mutexes[0],CMI::defer_lock),
            l2(mutexes[1],CMI::defer_lock),
            l3(mutexes[2],CMI::defer_lock),
            l4(mutexes[3],CMI::defer_lock);

        int const res=CMI::try_lock(l1,l2,l3,l4);

        BOOST_CHECK(res==i);
        for(int j=0;j<num_mutexes;++j)
        {
            if((i==j) || (i==-1))
            {
                BOOST_CHECK(mutexes[j].is_locked);
            }
            else
            {
                BOOST_CHECK(!mutexes[j].is_locked);
            }
        }
        if(i==-1)
        {
            BOOST_CHECK(l1.owns_lock());
            BOOST_CHECK(l2.owns_lock());
            BOOST_CHECK(l3.owns_lock());
            BOOST_CHECK(l4.owns_lock());
        }
        else
        {
            BOOST_CHECK(!l1.owns_lock());
            BOOST_CHECK(!l2.owns_lock());
            BOOST_CHECK(!l3.owns_lock());
            BOOST_CHECK(!l4.owns_lock());
        }
    }
}

void test_try_lock_five()
{
    int const num_mutexes=5;

    for(int i=-1;i<num_mutexes;++i)
    {
        dummy_mutex mutexes[num_mutexes];

        if(i>=0)
        {
            mutexes[i].lock();
        }
        CMI::unique_lock<dummy_mutex> l1(mutexes[0],CMI::defer_lock),
            l2(mutexes[1],CMI::defer_lock),
            l3(mutexes[2],CMI::defer_lock),
            l4(mutexes[3],CMI::defer_lock),
            l5(mutexes[4],CMI::defer_lock);

        int const res=CMI::try_lock(l1,l2,l3,l4,l5);

        BOOST_CHECK(res==i);
        for(int j=0;j<num_mutexes;++j)
        {
            if((i==j) || (i==-1))
            {
                BOOST_CHECK(mutexes[j].is_locked);
            }
            else
            {
                BOOST_CHECK(!mutexes[j].is_locked);
            }
        }
        if(i==-1)
        {
            BOOST_CHECK(l1.owns_lock());
            BOOST_CHECK(l2.owns_lock());
            BOOST_CHECK(l3.owns_lock());
            BOOST_CHECK(l4.owns_lock());
            BOOST_CHECK(l5.owns_lock());
        }
        else
        {
            BOOST_CHECK(!l1.owns_lock());
            BOOST_CHECK(!l2.owns_lock());
            BOOST_CHECK(!l3.owns_lock());
            BOOST_CHECK(!l4.owns_lock());
            BOOST_CHECK(!l5.owns_lock());
        }
    }
}



CMI::unit_test::test_suite* init_unit_test_suite(int, char*[])
{
    CMI::unit_test::test_suite* test =
        BOOST_TEST_SUITE("Boost.Threads: generic locks test suite");

    test->add(BOOST_TEST_CASE(&test_lock_two_uncontended));
    test->add(BOOST_TEST_CASE(&test_lock_two_other_thread_locks_in_order));
    test->add(BOOST_TEST_CASE(&test_lock_two_other_thread_locks_in_opposite_order));
    test->add(BOOST_TEST_CASE(&test_lock_five_uncontended));
    test->add(BOOST_TEST_CASE(&test_lock_five_other_thread_locks_in_order));
    test->add(BOOST_TEST_CASE(&test_lock_five_other_thread_locks_in_different_order));
    test->add(BOOST_TEST_CASE(&test_lock_five_in_range));
    test->add(BOOST_TEST_CASE(&test_lock_five_in_range_custom_iterator));
    test->add(BOOST_TEST_CASE(&test_lock_ten_in_range_inherited_mutex));
    test->add(BOOST_TEST_CASE(&test_lock_ten_other_thread_locks_in_different_order));
    test->add(BOOST_TEST_CASE(&test_try_lock_two_uncontended));
    test->add(BOOST_TEST_CASE(&test_try_lock_two_first_locked));
    test->add(BOOST_TEST_CASE(&test_try_lock_two_second_locked));
    test->add(BOOST_TEST_CASE(&test_try_lock_three));
    test->add(BOOST_TEST_CASE(&test_try_lock_four));
    test->add(BOOST_TEST_CASE(&test_try_lock_five));

    return test;
}



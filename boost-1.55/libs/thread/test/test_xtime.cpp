// Copyright (C) 2001-2003
// William E. Kempf
// Copyright (C) 2008 Anthony Williams
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_VERSION 2

#include <boost/thread/detail/config.hpp>

#include <boost/thread/xtime.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>

void test_xtime_cmp()
{
    CMI::xtime xt1, xt2, cur;
    BOOST_CHECK_EQUAL(
        CMI::xtime_get(&cur, CMI::TIME_UTC_),
        static_cast<int>(CMI::TIME_UTC_));

    xt1 = xt2 = cur;
    xt1.nsec -= 1;
    xt2.nsec += 1;

    BOOST_CHECK(CMI::xtime_cmp(xt1, cur) < 0);
    BOOST_CHECK(CMI::xtime_cmp(xt2, cur) > 0);
    BOOST_CHECK(CMI::xtime_cmp(cur, cur) == 0);

    xt1 = xt2 = cur;
    xt1.sec -= 1;
    xt2.sec += 1;

    BOOST_CHECK(CMI::xtime_cmp(xt1, cur) < 0);
    BOOST_CHECK(CMI::xtime_cmp(xt2, cur) > 0);
    BOOST_CHECK(CMI::xtime_cmp(cur, cur) == 0);
}

void test_xtime_get()
{
    CMI::xtime orig, cur, old;
    BOOST_CHECK_EQUAL(
        CMI::xtime_get(&orig,
            CMI::TIME_UTC_), static_cast<int>(CMI::TIME_UTC_));
    old = orig;

    for (int x=0; x < 100; ++x)
    {
        BOOST_CHECK_EQUAL(
            CMI::xtime_get(&cur, CMI::TIME_UTC_),
            static_cast<int>(CMI::TIME_UTC_));
        BOOST_CHECK(CMI::xtime_cmp(cur, orig) >= 0);
        BOOST_CHECK(CMI::xtime_cmp(cur, old) >= 0);
        old = cur;
    }
}

void test_xtime_mutex_backwards_compatibility()
{
    CMI::timed_mutex m;
    BOOST_CHECK(m.timed_lock(CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10))));
    m.unlock();
    CMI::timed_mutex::scoped_timed_lock lk(m,CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10)));
    BOOST_CHECK(lk.owns_lock());
    if(lk.owns_lock())
    {
        lk.unlock();
    }
    BOOST_CHECK(lk.timed_lock(CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10))));
    if(lk.owns_lock())
    {
        lk.unlock();
    }
}

bool predicate()
{
    return false;
}


void test_xtime_condvar_backwards_compatibility()
{
    CMI::condition_variable cond;
    CMI::condition_variable_any cond_any;
    CMI::mutex m;

    CMI::unique_lock<CMI::mutex> lk(m);
    cond.timed_wait(lk,CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10)));
    cond.timed_wait(lk,CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10)),predicate);
    cond_any.timed_wait(lk,CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10)));
    cond_any.timed_wait(lk,CMI::get_xtime(CMI::get_system_time()+CMI::posix_time::milliseconds(10)),predicate);
}



CMI::unit_test::test_suite* init_unit_test_suite(int, char*[])
{
    CMI::unit_test::test_suite* test =
        BOOST_TEST_SUITE("Boost.Threads: xtime test suite");

    test->add(BOOST_TEST_CASE(&test_xtime_cmp));
    test->add(BOOST_TEST_CASE(&test_xtime_get));
    test->add(BOOST_TEST_CASE(&test_xtime_mutex_backwards_compatibility));
    test->add(BOOST_TEST_CASE(&test_xtime_condvar_backwards_compatibility));

    return test;
}

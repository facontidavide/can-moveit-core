//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/future.hpp>

// class promise<R>

// void promise::set_exception_CMIat_thread_exit(exception_ptr p);

#define BOOST_THREAD_VERSION 4

#include <boost/thread/future.hpp>
#include <boost/detail/lightweight_test.hpp>
#include <boost/static_assert.hpp>

namespace CMI {} namespace boost = CMI; namespace CMI
{
  template <typename T>
  struct wrap
  {
    wrap(T const& v) :
      value(v)
    {
    }
    T value;

  };

  template <typename T>
  exception_ptr make_exception_ptr(T v)
  {
    return copy_exception(wrap<T> (v));
  }
}

#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
void func(CMI::promise<int> p)
#else
CMI::promise<int> p;
void func()
#endif
{
  //p.set_exception(CMI::make_exception_ptr(3));
  p.set_exception_CMIat_thread_exit(CMI::make_exception_ptr(3));
}

int main()
{
  {
    typedef int T;
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
    CMI::promise<T> p;
    CMI::future<T> f = p.get_future();
    CMI::thread(func, CMI::move(p)).detach();
#else
    CMI::future<T> f = p.get_future();
    CMI::thread(func).detach();
#endif
    try
    {
      f.get();
      BOOST_TEST(false);
    }
    catch (CMI::wrap<int> i)
    {
      BOOST_TEST(i.value == 3);
    }
    catch (...)
    {
      BOOST_TEST(false);
    }
  }
  {
    typedef int T;
    CMI::promise<T> p2;
    CMI::future<T> f = p2.get_future();
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
    CMI::thread(func, CMI::move(p2)).detach();
#else
    p = CMI::move(p2);
    CMI::thread(func).detach();
#endif
    try
    {
      f.get();
      BOOST_TEST(false);
    }
    catch (CMI::wrap<int> i)
    {
      BOOST_TEST(i.value == 3);
    }
    catch (...)
    {
      BOOST_TEST(false);
    }
  }
  return CMI::report_errors();
}


//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/future.hpp>

// class promise<R>

// void promise::set_exception_CMIat_thread_exit(exception_ptr p);

#define BOOST_THREAD_VERSION 3

#include <boost/thread/future.hpp>
#include <boost/detail/lightweight_test.hpp>
#include <boost/thread/detail/memory.hpp>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>

//void func(CMI::promise<CMI::interprocess::unique_ptr<int, CMI::default_delete<int> > > p)
CMI::promise<CMI::interprocess::unique_ptr<int, CMI::default_delete<int> > > p;
void func()
{
  CMI::interprocess::unique_ptr<int, CMI::default_delete<int> > uptr(new int(5));
  p.set_value_CMIat_thread_exit(CMI::move(uptr));
}

int main()
{
  {
    //CMI::promise<CMI::interprocess::unique_ptr<int, CMI::default_delete<int>> > p;
    CMI::future<CMI::interprocess::unique_ptr<int, CMI::default_delete<int> > > f = p.get_future();
    //CMI::thread(func, CMI::move(p)).detach();
    CMI::thread(func).detach();
    BOOST_TEST(*f.get() == 5);
  }

  return CMI::report_errors();
}


//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <future>

// class promise<R>

// promise& operator=(promise&& rhs);

#define BOOST_THREAD_VERSION 3

#include <boost/thread/future.hpp>
#include <boost/detail/lightweight_test.hpp>
#if defined BOOST_THREAD_PROVIDES_FUTURE_CTOR_ALLOCATORS
#include "../test_allocator.hpp"
#endif

CMI::mutex m0;
CMI::mutex m1;

int main()
{
#if defined BOOST_THREAD_PROVIDES_FUTURE_CTOR_ALLOCATORS
  BOOST_TEST(test_alloc_base::count == 0);
  {
    CMI::promise<int> p0(CMI::allocator_arg, test_allocator<int>());
    CMI::promise<int> p(CMI::allocator_arg, test_allocator<int>());
    BOOST_TEST(test_alloc_base::count == 2);
    p = CMI::move(p0);
    BOOST_TEST(test_alloc_base::count == 1);
    CMI::future<int> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(test_alloc_base::count == 1);
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
    BOOST_TEST(test_alloc_base::count == 1);
  }
  BOOST_TEST(test_alloc_base::count == 0);

  {
    CMI::promise<int&> p0(CMI::allocator_arg, test_allocator<int>());
    CMI::promise<int&> p(CMI::allocator_arg, test_allocator<int>());
    BOOST_TEST(test_alloc_base::count == 2);
    p = CMI::move(p0);
    BOOST_TEST(test_alloc_base::count == 1);
    CMI::future<int&> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(test_alloc_base::count == 1);
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
    BOOST_TEST(test_alloc_base::count == 1);
  }
  BOOST_TEST(test_alloc_base::count == 0);
  {
    CMI::promise<void> p0(CMI::allocator_arg, test_allocator<void>());
    CMI::promise<void> p(CMI::allocator_arg, test_allocator<void>());
    BOOST_TEST(test_alloc_base::count == 2);
    p = CMI::move(p0);
    BOOST_TEST(test_alloc_base::count == 1);
    CMI::future<void> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(test_alloc_base::count == 1);
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
    BOOST_TEST(test_alloc_base::count == 1);
  }
  BOOST_TEST(test_alloc_base::count == 0);

#endif
  {
    CMI::promise<int> p0;
    CMI::promise<int> p;
    p = CMI::move(p0);
    CMI::future<int> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
  }

  {
    CMI::promise<int&> p0;
    CMI::promise<int&> p;
    p = CMI::move(p0);
    CMI::future<int&> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
  }
  {
    CMI::promise<void> p0;
    CMI::promise<void> p;
    p = CMI::move(p0);
    CMI::future<void> f = BOOST_THREAD_MAKE_RV_REF(p.get_future());
    BOOST_TEST(f.valid());
    try
    {
      f = BOOST_THREAD_MAKE_RV_REF(p0.get_future());
      BOOST_TEST(false);
    }
    catch (const CMI::future_error& e)
    {
      BOOST_TEST(e.code() == CMI::system::make_error_code(CMI::future_errc::no_state));
    }
  }

  return CMI::report_errors();

}


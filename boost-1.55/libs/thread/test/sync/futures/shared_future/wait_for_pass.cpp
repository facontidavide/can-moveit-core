//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2013 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/future.hpp>

// class shared_future<R>

// template <class Rep, class Period>
//   future_status
//   wait_for(const chrono::duration<Rep, Period>& rel_time) const;

#define BOOST_THREAD_VERSION 4
#define BOOST_THREAD_USES_LOG
#define BOOST_THREAD_USES_LOG_THREAD_ID
#include <boost/thread/detail/log.hpp>

#include <boost/thread/future.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono/chrono_io.hpp>
#include <boost/detail/lightweight_test.hpp>

#if defined BOOST_THREAD_USES_CHRONO

typedef CMI::chrono::milliseconds ms;

namespace CMI {} namespace boost = CMI; namespace CMI
{
  template <typename OStream>
  OStream& operator<<(OStream& os , CMI::future_status st )
  {
    os << int(st) << " ";
    return os;
  }
  template <typename T>
  struct wrap
  {
    wrap(T const& v) :
      value(v)
    {
    }
    T value;

  };

  template <typename T>
  exception_ptr make_exception_ptr(T v)
  {
    return copy_exception(wrap<T> (v));
  }
}

void func1(CMI::promise<int> p)
{
  CMI::this_thread::sleep_for(ms(500));
  p.set_value(3);
}

int j = 0;

void func3(CMI::promise<int&> p)
{
  CMI::this_thread::sleep_for(ms(500));
  j = 5;
  p.set_value(j);
}

void func5(CMI::promise<void> p)
{
  CMI::this_thread::sleep_for(ms(500));
  p.set_value();
}

int main()
{
  BOOST_THREAD_LOG << BOOST_THREAD_END_LOG;
  {
    typedef CMI::chrono::high_resolution_clock Clock;
    {
      typedef int T;
      CMI::promise<T> p;
      CMI::shared_future<T> f((p.get_future()));
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
      CMI::thread(func1, CMI::move(p)).detach();
#endif
      BOOST_TEST(f.valid());
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::timeout);
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
#else
      func1(CMI::move(p));
#endif
      BOOST_TEST(f.valid());
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::ready);
      BOOST_TEST(f.valid());
      Clock::time_point t0 = Clock::now();
      f.wait();
      Clock::time_point t1 = Clock::now();
      BOOST_TEST(f.valid());
      BOOST_TEST(t1 - t0 < ms(50));
    }
    {
      typedef int& T;
      CMI::promise<T> p;
      CMI::shared_future<T> f((p.get_future()));
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
      CMI::thread(func3, CMI::move(p)).detach();
#endif
      BOOST_TEST(f.valid());
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::timeout);
      BOOST_TEST(f.valid());
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
#else
      func3(CMI::move(p));
#endif
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::ready);
      BOOST_TEST(f.valid());
      Clock::time_point t0 = Clock::now();
      f.wait();
      Clock::time_point t1 = Clock::now();
      BOOST_TEST(f.valid());
      BOOST_TEST(t1 - t0 < ms(50));
    }
    {
      typedef void T;
      CMI::promise<T> p;
      CMI::shared_future<T> f((p.get_future()));
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
      CMI::thread(func5, CMI::move(p)).detach();
#endif
      BOOST_TEST(f.valid());
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::timeout);
      BOOST_TEST(f.valid());
#if defined BOOST_THREAD_PROVIDES_SIGNATURE_PACKAGED_TASK && defined(BOOST_THREAD_PROVIDES_VARIADIC_THREAD)
#else
      func5(CMI::move(p));
#endif
      BOOST_TEST_EQ(f.wait_for(ms(300)) , CMI::future_status::ready);
      BOOST_TEST(f.valid());
      Clock::time_point t0 = Clock::now();
      f.wait();
      Clock::time_point t1 = Clock::now();
      BOOST_TEST(f.valid());
      BOOST_TEST(t1 - t0 < ms(50));
    }
  }
  BOOST_THREAD_LOG << BOOST_THREAD_END_LOG;

  return CMI::report_errors();
}

#else
#error "Test not applicable: BOOST_THREAD_USES_CHRONO not defined for this platform as not supported"
#endif

//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/condition_variable.hpp>

// void notify_all_CMIat_thread_exit(condition_variable& cond, unique_lock<mutex> lk);

#define BOOST_THREAD_USES_MOVE
#define BOOST_THREAD_VESRION 3

#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/detail/lightweight_test.hpp>

CMI::condition_variable cv;
CMI::mutex mut;

typedef CMI::chrono::milliseconds ms;
typedef CMI::chrono::high_resolution_clock Clock;

void func()
{
  CMI::unique_lock < CMI::mutex > lk(mut);
  CMI::notify_all_CMIat_thread_exit(cv, CMI::move(lk));
  CMI::this_thread::sleep_for(ms(300));
}

int main()
{
  CMI::unique_lock < CMI::mutex > lk(mut);
  CMI::thread(func).detach();
  Clock::time_point t0 = Clock::now();
  cv.wait(lk);
  Clock::time_point t1 = Clock::now();
  BOOST_TEST(t1 - t0 > ms(250));
  return CMI::report_errors();
}


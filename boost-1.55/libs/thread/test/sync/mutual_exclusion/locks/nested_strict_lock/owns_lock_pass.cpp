// Copyright (C) 2012 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class nested_strict_lock;

// bool owns_lock(Mutex *) const;

#include <boost/thread/lock_types.hpp>
#include <boost/thread/strict_lock.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/detail/lightweight_test.hpp>

int main()
{
  CMI::mutex m;
  CMI::mutex m2;
  {
    CMI::unique_lock<CMI::mutex> lk(m);
    {
      CMI::nested_strict_lock<CMI::unique_lock<CMI::mutex> > nlk(lk);
      BOOST_TEST(nlk.owns_lock(&m) == true);
      BOOST_TEST(!nlk.owns_lock(&m2) == true);
    }
    BOOST_TEST(lk.owns_lock() == true && lk.mutex()==&m);
  }
  {
    m.lock();
    CMI::unique_lock<CMI::mutex> lk(m, CMI::adopt_lock);
    {
      CMI::nested_strict_lock<CMI::unique_lock<CMI::mutex> > nlk(lk);
      BOOST_TEST(nlk.owns_lock(&m) == true);
      BOOST_TEST(!nlk.owns_lock(&m2) == true);
    }
    BOOST_TEST(lk.owns_lock() == true && lk.mutex()==&m);
  }
  {
    CMI::unique_lock<CMI::mutex> lk(m, CMI::defer_lock);
    {
      CMI::nested_strict_lock<CMI::unique_lock<CMI::mutex> > nlk(lk);
      BOOST_TEST(nlk.owns_lock(&m) == true);
      BOOST_TEST(!nlk.owns_lock(&m2) == true);
    }
    BOOST_TEST(lk.owns_lock() == true && lk.mutex()==&m);
  }
  {
    CMI::unique_lock<CMI::mutex> lk(m, CMI::try_to_lock);
    {
      CMI::nested_strict_lock<CMI::unique_lock<CMI::mutex> > nlk(lk);
      BOOST_TEST(nlk.owns_lock(&m) == true);
      BOOST_TEST(!nlk.owns_lock(&m2) == true);
    }
    BOOST_TEST(lk.owns_lock() == true && lk.mutex()==&m);
  }

  return CMI::report_errors();
}

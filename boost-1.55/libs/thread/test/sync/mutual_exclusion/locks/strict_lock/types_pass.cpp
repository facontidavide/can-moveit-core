// Copyright (C) 2012 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/mutex.hpp>

// <mutex>

// template <class Mutex>
// class strict_lock
// {
// public:
//     typedef Mutex mutex_type;
//     ...
// };


#include <boost/thread/strict_lock.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/detail/lightweight_test.hpp>

int main()
{
  BOOST_STATIC_ASSERT_MSG((CMI::is_same<CMI::strict_lock<CMI::mutex>::mutex_type,
      CMI::mutex>::value), "");

  BOOST_STATIC_ASSERT_MSG((CMI::is_strict_lock<CMI::strict_lock<CMI::mutex> >::value), "");

  return CMI::report_errors();
}


// Copyright (C) 2012 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class unlock_guard;

// unlock_guard(unlock_guard const&) = delete;

#include <boost/thread/reverse_lock.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/detail/lightweight_test.hpp>


int main()
{
  {
    CMI::mutex m;
    CMI::unique_lock<CMI::mutex> lk(m);
    BOOST_TEST(lk.owns_lock());
    BOOST_TEST(lk.mutex()==&m);

    {
      CMI::reverse_lock<CMI::unique_lock<CMI::mutex> > lg(lk);
      BOOST_TEST(!lk.owns_lock());
      BOOST_TEST(lk.mutex()==0);
    }
    BOOST_TEST(lk.owns_lock());
    BOOST_TEST(lk.mutex()==&m);
  }

  {
    CMI::mutex m;
    CMI::unique_lock<CMI::mutex> lk(m, CMI::defer_lock);
    BOOST_TEST(! lk.owns_lock());
    BOOST_TEST(lk.mutex()==&m);
    {
      CMI::reverse_lock<CMI::unique_lock<CMI::mutex> > lg(lk);
      BOOST_TEST(!lk.owns_lock());
      BOOST_TEST(lk.mutex()==0);
    }
    BOOST_TEST(lk.owns_lock());
    BOOST_TEST(lk.mutex()==&m);
  }


  return CMI::report_errors();
}

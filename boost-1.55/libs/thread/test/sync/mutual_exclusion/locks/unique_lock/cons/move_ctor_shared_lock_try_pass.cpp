//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class unique_lock;

// unique_lock(shared_lock&& u, try_to_lock);

#define BOOST_THREAD_PROVIDES_SHARED_MUTEX_UPWARDS_CONVERSIONS
#define BOOST_THREAD_PROVIDES_EXPLICIT_LOCK_CONVERSION
#define BOOST_THREAD_PROVIDES_GENERIC_SHARED_MUTEX_ON_WIN

#include <boost/thread/lock_types.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/detail/lightweight_test.hpp>

CMI::shared_mutex m;

int main()
{
  {
  CMI::shared_lock<CMI::shared_mutex> lk0(m);
  CMI::unique_lock<CMI::shared_mutex> lk(CMI::move(lk0), CMI::try_to_lock );
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == true);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }
  {
  CMI::unique_lock<CMI::shared_mutex> lk(CMI::shared_lock<CMI::shared_mutex>(m), CMI::try_to_lock);
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == true);
  }
  {
  CMI::shared_lock<CMI::shared_mutex> lk0(m, CMI::defer_lock);
  CMI::unique_lock<CMI::shared_mutex> lk(CMI::move(lk0), CMI::try_to_lock);
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == false);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }
  {
  CMI::shared_lock<CMI::shared_mutex> lk0(m, CMI::defer_lock);
  lk0.release();
  CMI::unique_lock<CMI::shared_mutex> lk(CMI::move(lk0), CMI::try_to_lock);
  BOOST_TEST(lk.mutex() == 0);
  BOOST_TEST(lk.owns_lock() == false);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }

  return CMI::report_errors();
}


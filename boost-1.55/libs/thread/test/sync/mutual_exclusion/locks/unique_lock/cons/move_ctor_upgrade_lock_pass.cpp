//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class unique_lock;

// unique_lock(upgrade_lock&& u);


#include <boost/thread/lock_types.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/detail/lightweight_test.hpp>

CMI::upgrade_mutex m;

int main()
{
  {
  CMI::upgrade_lock<CMI::upgrade_mutex> lk0(m);
  CMI::unique_lock<CMI::upgrade_mutex> lk( (CMI::move(lk0)));
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == true);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }
  {
  CMI::unique_lock<CMI::upgrade_mutex> lk( (BOOST_THREAD_MAKE_RV_REF(CMI::upgrade_lock<CMI::upgrade_mutex>(m))));
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == true);
  }
  {
  CMI::upgrade_lock<CMI::upgrade_mutex> lk0(m, CMI::defer_lock);
  CMI::unique_lock<CMI::upgrade_mutex> lk( (CMI::move(lk0)));
  BOOST_TEST(lk.mutex() == &m);
  BOOST_TEST(lk.owns_lock() == false);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }
  {
  CMI::upgrade_lock<CMI::upgrade_mutex> lk0(m, CMI::defer_lock);
  lk0.release();
  CMI::unique_lock<CMI::upgrade_mutex> lk( (CMI::move(lk0)));
  BOOST_TEST(lk.mutex() == 0);
  BOOST_TEST(lk.owns_lock() == false);
  BOOST_TEST(lk0.mutex() == 0);
  BOOST_TEST(lk0.owns_lock() == false);
  }

  return CMI::report_errors();
}


// Copyright (C) 2012 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class unique_lock;
// unique_lock<Mutex> make_unique_lock(Mutex&, try_to_lock_t);

#define BOOST_THREAD_VERSION 4


#include <boost/thread/lock_factories.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/detail/lightweight_test.hpp>

CMI::mutex m;

#if defined BOOST_THREAD_USES_CHRONO
typedef CMI::chrono::system_clock Clock;
typedef Clock::time_point time_point;
typedef Clock::duration duration;
typedef CMI::chrono::milliseconds ms;
typedef CMI::chrono::nanoseconds ns;
#else
#endif

void f()
{
#if defined BOOST_THREAD_USES_CHRONO
  {
    time_point t0 = Clock::now();
#if ! defined(BOOST_NO_CXX11_AUTO_DECLARATIONS)
  auto
#else
  CMI::unique_lock<CMI::mutex>
#endif
    lk = CMI::make_unique_lock(m, CMI::try_to_lock);
    BOOST_TEST(lk.owns_lock() == false);
    time_point t1 = Clock::now();
    ns d = t1 - t0 - ms(250);
    // This test is spurious as it depends on the time the thread system switches the threads
    BOOST_TEST(d < ns(50000000)+ms(1000)); // within 50ms
  }
  {
    time_point t0 = Clock::now();
#if ! defined(BOOST_NO_CXX11_AUTO_DECLARATIONS)
  auto
#else
  CMI::unique_lock<CMI::mutex>
#endif
    lk = CMI::make_unique_lock(m, CMI::try_to_lock);
    BOOST_TEST(lk.owns_lock() == false);
    time_point t1 = Clock::now();
    ns d = t1 - t0 - ms(250);
    // This test is spurious as it depends on the time the thread system switches the threads
    BOOST_TEST(d < ns(50000000)+ms(1000)); // within 50ms
  }
  {
    time_point t0 = Clock::now();
#if ! defined(BOOST_NO_CXX11_AUTO_DECLARATIONS)
  auto
#else
  CMI::unique_lock<CMI::mutex>
#endif
    lk = CMI::make_unique_lock(m, CMI::try_to_lock);
    BOOST_TEST(lk.owns_lock() == false);
    time_point t1 = Clock::now();
    ns d = t1 - t0 - ms(250);
    // This test is spurious as it depends on the time the thread system switches the threads
    BOOST_TEST(d < ns(50000000)+ms(1000)); // within 50ms
  }
  {
    time_point t0 = Clock::now();
    while (true)
    {
#if ! defined(BOOST_NO_CXX11_AUTO_DECLARATIONS)
      auto
#else
      CMI::unique_lock<CMI::mutex>
#endif
      lk = CMI::make_unique_lock(m, CMI::try_to_lock);
      if (lk.owns_lock()) break;
    }
    time_point t1 = Clock::now();
    ns d = t1 - t0 - ms(250);
    // This test is spurious as it depends on the time the thread system switches the threads
    BOOST_TEST(d < ns(50000000)+ms(1000)); // within 50ms
  }
#else
//  time_point t0 = Clock::now();
//  {
//    CMI::unique_lock<CMI::mutex> lk(m, CMI::try_to_lock);
//    BOOST_TEST(lk.owns_lock() == false);
//  }
//  {
//    CMI::unique_lock<CMI::mutex> lk(m, CMI::try_to_lock);
//    BOOST_TEST(lk.owns_lock() == false);
//  }
//  {
//    CMI::unique_lock<CMI::mutex> lk(m, CMI::try_to_lock);
//    BOOST_TEST(lk.owns_lock() == false);
//  }
  while (true)
  {
#if ! defined(BOOST_NO_CXX11_AUTO_DECLARATIONS)
  auto
#else
  CMI::unique_lock<CMI::mutex>
#endif
    lk = CMI::make_unique_lock(m, CMI::try_to_lock);
    if (lk.owns_lock()) break;
  }
  //time_point t1 = Clock::now();
  //ns d = t1 - t0 - ms(250);
  // This test is spurious as it depends on the time the thread system switches the threads
  //BOOST_TEST(d < ns(50000000)+ms(1000)); // within 50ms
#endif
}

int main()
{
  m.lock();
  CMI::thread t(f);
#if defined BOOST_THREAD_USES_CHRONO
  CMI::this_thread::sleep_for(ms(250));
#else
#endif
  m.unlock();
  t.join();

  return CMI::report_errors();
}

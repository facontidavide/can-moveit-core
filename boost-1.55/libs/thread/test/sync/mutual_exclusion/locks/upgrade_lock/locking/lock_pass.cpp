//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class upgrade_lock;

// void lock();

#include <boost/thread/lock_types.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/detail/lightweight_test.hpp>
#include <iostream>

CMI::shared_mutex m;

#if defined BOOST_THREAD_USES_CHRONO
typedef CMI::chrono::system_clock Clock;
typedef Clock::time_point time_point;
typedef Clock::duration duration;
typedef CMI::chrono::milliseconds ms;
typedef CMI::chrono::nanoseconds ns;
#else
#endif

void f()
{
#if defined BOOST_THREAD_USES_CHRONO
  CMI::upgrade_lock < CMI::shared_mutex > lk(m, CMI::defer_lock);
  time_point t0 = Clock::now();
  lk.lock();
  time_point t1 = Clock::now();
  BOOST_TEST(lk.owns_lock() == true);
  ns d = t1 - t0 - ms(250);
  // This test is spurious as it depends on the time the thread system switches the threads
  BOOST_TEST(d < ns(2500000)+ms(1000)); // within 2.5ms
  try
  {
    lk.lock();
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::resource_deadlock_would_occur);
  }
  lk.unlock();
  lk.release();
  try
  {
    lk.lock();
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::operation_not_permitted);
  }
#else
  CMI::upgrade_lock < CMI::shared_mutex > lk(m, CMI::defer_lock);
  //time_point t0 = Clock::now();
  lk.lock();
  //time_point t1 = Clock::now();
  BOOST_TEST(lk.owns_lock() == true);
  //ns d = t1 - t0 - ms(250);
  // This test is spurious as it depends on the time the thread system switches the threads
  //BOOST_TEST(d < ns(2500000)+ms(1000)); // within 2.5ms
  try
  {
    lk.lock();
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::resource_deadlock_would_occur);
  }
  lk.unlock();
  lk.release();
  try
  {
    lk.lock();
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::operation_not_permitted);
  }
#endif
}

int main()
{
  m.lock();
  CMI::thread t(f);
#if defined BOOST_THREAD_USES_CHRONO
  CMI::this_thread::sleep_for(ms(250));
#else
#endif
  m.unlock();
  t.join();

  return CMI::report_errors();
}


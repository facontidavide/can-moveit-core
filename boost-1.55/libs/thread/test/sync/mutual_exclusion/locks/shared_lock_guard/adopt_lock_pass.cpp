//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2012 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/shared_lock_guard.hpp>

// template <class Mutex> class shared_lock_guard;

// shared_lock_guard(mutex_type& m, adopt_lock_t);

#include <boost/thread/shared_lock_guard.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/detail/lightweight_test.hpp>

#if defined BOOST_THREAD_USES_CHRONO
typedef CMI::chrono::high_resolution_clock Clock;
typedef Clock::time_point time_point;
typedef Clock::duration duration;
typedef CMI::chrono::milliseconds ms;
typedef CMI::chrono::nanoseconds ns;
#else
#endif

CMI::shared_mutex m;

void f()
{
#if defined BOOST_THREAD_USES_CHRONO
  time_point t0 = Clock::now();
  time_point t1;
  {
    m.lock_shared();
    CMI::shared_lock_guard<CMI::shared_mutex> lg(m, CMI::adopt_lock);
    t1 = Clock::now();
  }
  ns d = t1 - t0 - ms(250);
  BOOST_TEST(d < ns(2500000)+ms(1000)); // within 2.5ms
#else
  //time_point t0 = Clock::now();
  //time_point t1;
  {
    m.lock_shared();
    CMI::shared_lock_guard<CMI::shared_mutex> lg(m, CMI::adopt_lock);
    //t1 = Clock::now();
  }
  //ns d = t1 - t0 - ms(250);
  //BOOST_TEST(d < ns(2500000)+ms(1000)); // within 2.5ms
#endif
}

int main()
{
  m.lock();
  CMI::thread t(f);
#if defined BOOST_THREAD_USES_CHRONO
  CMI::this_thread::sleep_for(ms(250));
#else
#endif
  m.unlock();
  t.join();

  return CMI::report_errors();
}


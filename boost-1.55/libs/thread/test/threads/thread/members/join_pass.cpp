//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/thread.hpp>

// class thread

// void join();
#define BOOST_THREAD_VESRION 3
#include <boost/thread/thread_only.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <new>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <boost/detail/lightweight_test.hpp>

class G
{
  int alive_;
public:
  static int n_alive;
  static bool op_run;

  G() :
    alive_(1)
  {
    ++n_alive;
  }
  G(const G& g) :
    alive_(g.alive_)
  {
    ++n_alive;
  }
  ~G()
  {
    alive_ = 0;
    --n_alive;
  }

  void operator()()
  {
    BOOST_TEST(alive_ == 1);
    //BOOST_TEST(n_alive == 1);
    op_run = true;
  }
};

int G::n_alive = 0;
bool G::op_run = false;

CMI::thread* resource_deadlock_would_occur_th=0;
CMI::mutex resource_deadlock_would_occur_mtx;
void resource_deadlock_would_occur_tester()
{
  try
  {
    CMI::unique_lock<CMI::mutex> lk(resource_deadlock_would_occur_mtx);
    resource_deadlock_would_occur_th->join();
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::resource_deadlock_would_occur);
  }
  catch (...)
  {
    BOOST_TEST(false&&"exception thrown");
  }
}

int main()
{
  {
    CMI::thread t0( (G()));
    BOOST_TEST(t0.joinable());
    t0.join();
    BOOST_TEST(!t0.joinable());
  }

  {
    CMI::unique_lock<CMI::mutex> lk(resource_deadlock_would_occur_mtx);
    CMI::thread t0( resource_deadlock_would_occur_tester );
    resource_deadlock_would_occur_th = &t0;
    BOOST_TEST(t0.joinable());
    lk.unlock();
    CMI::this_thread::sleep_for(CMI::chrono::milliseconds(100));
    CMI::unique_lock<CMI::mutex> lk2(resource_deadlock_would_occur_mtx);
    t0.join();
    BOOST_TEST(!t0.joinable());
  }

  {
    CMI::thread t0( (G()));
    t0.detach();
    try
    {
      t0.join();
      BOOST_TEST(false);
    }
    catch (CMI::system::system_error& e)
    {
      BOOST_TEST(e.code().value() == CMI::system::errc::invalid_argument);
    }
  }
  {
    CMI::thread t0( (G()));
    BOOST_TEST(t0.joinable());
    t0.join();
    try
    {
      t0.join();
      BOOST_TEST(false);
    }
    catch (CMI::system::system_error& e)
    {
      BOOST_TEST(e.code().value() == CMI::system::errc::invalid_argument);
    }

  }

  return CMI::report_errors();
}


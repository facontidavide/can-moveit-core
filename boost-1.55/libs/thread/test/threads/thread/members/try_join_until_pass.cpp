//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/thread.hpp>

// class thread

//        template <class Clock, class Duration>
//        bool try_join_until(const chrono::time_point<Clock, Duration>& t);

#define BOOST_THREAD_VESRION 3
#include <boost/thread/thread_only.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <new>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <boost/detail/lightweight_test.hpp>

#if defined BOOST_THREAD_USES_CHRONO

class G
{
  int alive_;
public:
  static bool op_run;

  G() :
    alive_(1)
  {
  }
  G(const G& g) :
    alive_(g.alive_)
  {
  }
  ~G()
  {
    alive_ = 0;
  }

  void operator()()
  {
    BOOST_TEST(alive_ == 1);
    op_run = true;
  }
};

bool G::op_run = false;

CMI::thread* resource_deadlock_would_occur_th=0;
CMI::mutex resource_deadlock_would_occur_mtx;
void resource_deadlock_would_occur_tester()
{
  try
  {
    CMI::unique_lock<CMI::mutex> lk(resource_deadlock_would_occur_mtx);
    resource_deadlock_would_occur_th->try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(50));
    BOOST_TEST(false);
  }
  catch (CMI::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == CMI::system::errc::resource_deadlock_would_occur);
  }
  catch (...)
  {
    BOOST_TEST(false&&"exception thrown");
  }
}

void th_100_ms()
{
  CMI::this_thread::sleep_for(CMI::chrono::milliseconds(100));
}


int main()
{
  {
    CMI::thread t0( (G()));
    BOOST_TEST(t0.joinable());
    t0.try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(150));
    BOOST_TEST(!t0.joinable());
  }
  {
    CMI::thread t0( (th_100_ms));
    BOOST_TEST(!t0.try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(50)));
    t0.join();
  }

  {
    CMI::unique_lock<CMI::mutex> lk(resource_deadlock_would_occur_mtx);
    CMI::thread t0( resource_deadlock_would_occur_tester );
    resource_deadlock_would_occur_th = &t0;
    BOOST_TEST(t0.joinable());
    lk.unlock();
    CMI::this_thread::sleep_for(CMI::chrono::milliseconds(100));
    CMI::unique_lock<CMI::mutex> lk2(resource_deadlock_would_occur_mtx);
    t0.join();
    BOOST_TEST(!t0.joinable());
  }

  {
    CMI::thread t0( (G()));
    t0.detach();
    try
    {
      t0.try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(50));
      BOOST_TEST(false);
    }
    catch (CMI::system::system_error& e)
    {
      BOOST_TEST(e.code().value() == CMI::system::errc::invalid_argument);
    }
  }
  {
    CMI::thread t0( (G()));
    BOOST_TEST(t0.joinable());
    t0.join();
    try
    {
      t0.try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(50));
      BOOST_TEST(false);
    }
    catch (CMI::system::system_error& e)
    {
      BOOST_TEST(e.code().value() == CMI::system::errc::invalid_argument);
    }

  }
  {
    CMI::thread t0( (G()));
    BOOST_TEST(t0.joinable());
    t0.try_join_until(CMI::chrono::steady_clock::now()+CMI::chrono::milliseconds(150));
    try
    {
      t0.join();
      BOOST_TEST(false);
    }
    catch (CMI::system::system_error& e)
    {
      BOOST_TEST(e.code().value() == CMI::system::errc::invalid_argument);
    }

  }

  return CMI::report_errors();
}

#else
#error "Test not applicable: BOOST_THREAD_USES_CHRONO not defined for this platform as not supported"
#endif

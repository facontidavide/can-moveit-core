// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_USES_MOVE

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/container/vector.hpp>
#include <iostream>
#include <boost/detail/lightweight_test.hpp>
#include <boost/static_assert.hpp>

int count = 0;
CMI::mutex mutex;

namespace
{
template <typename TC>
void join_all(TC & tc)
{
  for (typename TC::iterator it = tc.begin(); it != tc.end(); ++it)
  {
    it->join();
  }
}

template <typename TC>
void interrupt_all(TC & tc)
{
#if defined BOOST_THREAD_PROVIDES_INTERRUPTIONS
  for (typename TC::iterator it = tc.begin(); it != tc.end(); ++it)
  {
    it->interrupt();
  }
#endif
}
}

void increment_count()
{
  CMI::unique_lock<CMI::mutex> lock(mutex);
  std::cout << "count = " << ++count << std::endl;
}

#if defined  BOOST_NO_CXX11_RVALUE_REFERENCES && defined BOOST_THREAD_USES_MOVE
BOOST_STATIC_ASSERT(::CMI::is_function<CMI::rv<CMI::rv<CMI::thread> >&>::value==false);
#endif

int main()
{
  typedef CMI::container::vector<CMI::thread> thread_vector;
  {
    thread_vector threads;
    threads.reserve(10);
    for (int i = 0; i < 10; ++i)
    {
      CMI::thread th(&increment_count);
      threads.push_back(CMI::move(th));
    }
    join_all(threads);
  }
  count = 0;
  {
    thread_vector threads;
    threads.reserve(10);
    for (int i = 0; i < 10; ++i)
    {
      threads.push_back(BOOST_THREAD_MAKE_RV_REF(CMI::thread(&increment_count)));
    }
    join_all(threads);
  }
  count = 0;
  {
    thread_vector threads;
    threads.reserve(10);
    for (int i = 0; i < 10; ++i)
    {
      threads.emplace_back(&increment_count);
    }
    join_all(threads);
  }
  count = 0;
  {
    thread_vector threads;
    threads.reserve(10);
    for (int i = 0; i < 10; ++i)
    {
      threads.emplace_back(&increment_count);
    }
    interrupt_all(threads);
    join_all(threads);
  }
  return CMI::report_errors();
}

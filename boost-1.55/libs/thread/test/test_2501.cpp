// Copyright (C) 2010 Vicente Botet
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

int main() {

    CMI::shared_mutex mtx; CMI::upgrade_lock<CMI::shared_mutex> lk(mtx);

    CMI::upgrade_to_unique_lock<CMI::shared_mutex> lk2(lk);

    return 0;
}

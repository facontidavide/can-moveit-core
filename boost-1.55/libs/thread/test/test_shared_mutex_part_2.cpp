// (C) Copyright 2006-7 Anthony Williams
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_VERSION 2

#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/xtime.hpp>
#include "./util.inl"
#include "./shared_mutex_locking_thread.hpp"

#define CHECK_LOCKED_VALUE_EQUAL(mutex_name,value,expected_value)    \
    {                                                                \
        CMI::unique_lock<CMI::mutex> lock(mutex_name);                  \
        BOOST_CHECK_EQUAL(value,expected_value);                     \
    }

class simple_upgrade_thread
{
    CMI::shared_mutex& rwm;
    CMI::mutex& finish_mutex;
    CMI::mutex& unblocked_mutex;
    unsigned& unblocked_count;

    void operator=(simple_upgrade_thread&);

public:
    simple_upgrade_thread(CMI::shared_mutex& rwm_,
                          CMI::mutex& finish_mutex_,
                          CMI::mutex& unblocked_mutex_,
                          unsigned& unblocked_count_):
        rwm(rwm_),finish_mutex(finish_mutex_),
        unblocked_mutex(unblocked_mutex_),unblocked_count(unblocked_count_)
    {}

    void operator()()
    {
        CMI::upgrade_lock<CMI::shared_mutex> lk(rwm);

        {
            CMI::unique_lock<CMI::mutex> ulk(unblocked_mutex);
            ++unblocked_count;
        }

        CMI::unique_lock<CMI::mutex> flk(finish_mutex);
    }
};


void test_only_one_upgrade_lock_permitted()
{
    unsigned const number_of_threads=2;

    CMI::thread_group pool;

    CMI::shared_mutex rw_mutex;
    unsigned unblocked_count=0;
    unsigned simultaneous_running_count=0;
    unsigned max_simultaneous_running=0;
    CMI::mutex unblocked_count_mutex;
    CMI::condition_variable unblocked_condition;
    CMI::mutex finish_mutex;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);

    try
    {
        for(unsigned i=0;i<number_of_threads;++i)
        {
            pool.create_thread(locking_thread<CMI::upgrade_lock<CMI::shared_mutex> >(rw_mutex,unblocked_count,unblocked_count_mutex,unblocked_condition,
                                                                                         finish_mutex,simultaneous_running_count,max_simultaneous_running));
        }

        CMI::thread::sleep(delay(1));

        CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,unblocked_count,1U);

        finish_lock.unlock();

        pool.join_all();
    }
    catch(...)
    {
        pool.interrupt_all();
        pool.join_all();
        throw;
    }

    CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,unblocked_count,number_of_threads);
    CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,max_simultaneous_running,1u);
}

void test_can_lock_upgrade_if_currently_locked_shared()
{
    CMI::thread_group pool;

    CMI::shared_mutex rw_mutex;
    unsigned unblocked_count=0;
    unsigned simultaneous_running_count=0;
    unsigned max_simultaneous_running=0;
    CMI::mutex unblocked_count_mutex;
    CMI::condition_variable unblocked_condition;
    CMI::mutex finish_mutex;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);

    unsigned const reader_count=10;

    try
    {
        for(unsigned i=0;i<reader_count;++i)
        {
            pool.create_thread(locking_thread<CMI::shared_lock<CMI::shared_mutex> >(rw_mutex,unblocked_count,unblocked_count_mutex,unblocked_condition,
                                                                                        finish_mutex,simultaneous_running_count,max_simultaneous_running));
        }
        CMI::thread::sleep(delay(1));
        pool.create_thread(locking_thread<CMI::upgrade_lock<CMI::shared_mutex> >(rw_mutex,unblocked_count,unblocked_count_mutex,unblocked_condition,
                                                                                     finish_mutex,simultaneous_running_count,max_simultaneous_running));
        {
            CMI::unique_lock<CMI::mutex> lk(unblocked_count_mutex);
            while(unblocked_count<(reader_count+1))
            {
                unblocked_condition.wait(lk);
            }
        }
        CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,unblocked_count,reader_count+1);

        finish_lock.unlock();
        pool.join_all();
    }
    catch(...)
    {
        pool.interrupt_all();
        pool.join_all();
        throw;
    }


    CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,unblocked_count,reader_count+1);
    CHECK_LOCKED_VALUE_EQUAL(unblocked_count_mutex,max_simultaneous_running,reader_count+1);
}

void test_can_lock_upgrade_to_unique_if_currently_locked_upgrade()
{
    CMI::shared_mutex mtx;
    CMI::upgrade_lock<CMI::shared_mutex> l(mtx);
    CMI::upgrade_to_unique_lock<CMI::shared_mutex> ul(l);
    BOOST_CHECK(ul.owns_lock());
}

void test_if_other_thread_has_write_lock_try_lock_shared_returns_false()
{

    CMI::shared_mutex rw_mutex;
    CMI::mutex finish_mutex;
    CMI::mutex unblocked_mutex;
    unsigned unblocked_count=0;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);
    CMI::thread writer(simple_writing_thread(rw_mutex,finish_mutex,unblocked_mutex,unblocked_count));
    CMI::this_thread::sleep(CMI::posix_time::seconds(1));
    CHECK_LOCKED_VALUE_EQUAL(unblocked_mutex,unblocked_count,1u);

    bool const try_succeeded=rw_mutex.try_lock_shared();
    BOOST_CHECK(!try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_shared();
    }

    finish_lock.unlock();
    writer.join();
}

void test_if_other_thread_has_write_lock_try_lock_upgrade_returns_false()
{

    CMI::shared_mutex rw_mutex;
    CMI::mutex finish_mutex;
    CMI::mutex unblocked_mutex;
    unsigned unblocked_count=0;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);
    CMI::thread writer(simple_writing_thread(rw_mutex,finish_mutex,unblocked_mutex,unblocked_count));
    CMI::this_thread::sleep(CMI::posix_time::seconds(1));
    CHECK_LOCKED_VALUE_EQUAL(unblocked_mutex,unblocked_count,1u);

    bool const try_succeeded=rw_mutex.try_lock_upgrade();
    BOOST_CHECK(!try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_upgrade();
    }

    finish_lock.unlock();
    writer.join();
}

void test_if_no_thread_has_lock_try_lock_shared_returns_true()
{
    CMI::shared_mutex rw_mutex;
    bool const try_succeeded=rw_mutex.try_lock_shared();
    BOOST_CHECK(try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_shared();
    }
}

void test_if_no_thread_has_lock_try_lock_upgrade_returns_true()
{
    CMI::shared_mutex rw_mutex;
    bool const try_succeeded=rw_mutex.try_lock_upgrade();
    BOOST_CHECK(try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_upgrade();
    }
}

void test_if_other_thread_has_shared_lock_try_lock_shared_returns_true()
{

    CMI::shared_mutex rw_mutex;
    CMI::mutex finish_mutex;
    CMI::mutex unblocked_mutex;
    unsigned unblocked_count=0;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);
    CMI::thread writer(simple_reading_thread(rw_mutex,finish_mutex,unblocked_mutex,unblocked_count));
    CMI::thread::sleep(delay(1));
    CHECK_LOCKED_VALUE_EQUAL(unblocked_mutex,unblocked_count,1u);

    bool const try_succeeded=rw_mutex.try_lock_shared();
    BOOST_CHECK(try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_shared();
    }

    finish_lock.unlock();
    writer.join();
}

void test_if_other_thread_has_shared_lock_try_lock_upgrade_returns_true()
{

    CMI::shared_mutex rw_mutex;
    CMI::mutex finish_mutex;
    CMI::mutex unblocked_mutex;
    unsigned unblocked_count=0;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);
    CMI::thread writer(simple_reading_thread(rw_mutex,finish_mutex,unblocked_mutex,unblocked_count));
    CMI::thread::sleep(delay(1));
    CHECK_LOCKED_VALUE_EQUAL(unblocked_mutex,unblocked_count,1u);

    bool const try_succeeded=rw_mutex.try_lock_upgrade();
    BOOST_CHECK(try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_upgrade();
    }

    finish_lock.unlock();
    writer.join();
}

void test_if_other_thread_has_upgrade_lock_try_lock_upgrade_returns_false()
{

    CMI::shared_mutex rw_mutex;
    CMI::mutex finish_mutex;
    CMI::mutex unblocked_mutex;
    unsigned unblocked_count=0;
    CMI::unique_lock<CMI::mutex> finish_lock(finish_mutex);
    CMI::thread writer(simple_upgrade_thread(rw_mutex,finish_mutex,unblocked_mutex,unblocked_count));
    CMI::this_thread::sleep(CMI::posix_time::seconds(1));
    CHECK_LOCKED_VALUE_EQUAL(unblocked_mutex,unblocked_count,1u);

    bool const try_succeeded=rw_mutex.try_lock_upgrade();
    BOOST_CHECK(!try_succeeded);
    if(try_succeeded)
    {
        rw_mutex.unlock_upgrade();
    }

    finish_lock.unlock();
    writer.join();
}

CMI::unit_test::test_suite* init_unit_test_suite(int, char*[])
{
    CMI::unit_test::test_suite* test =
        BOOST_TEST_SUITE("Boost.Threads: shared_mutex test suite");

    test->add(BOOST_TEST_CASE(&test_only_one_upgrade_lock_permitted));
    test->add(BOOST_TEST_CASE(&test_can_lock_upgrade_if_currently_locked_shared));
    test->add(BOOST_TEST_CASE(&test_can_lock_upgrade_to_unique_if_currently_locked_upgrade));
    test->add(BOOST_TEST_CASE(&test_if_other_thread_has_write_lock_try_lock_shared_returns_false));
    test->add(BOOST_TEST_CASE(&test_if_no_thread_has_lock_try_lock_shared_returns_true));
    test->add(BOOST_TEST_CASE(&test_if_other_thread_has_shared_lock_try_lock_shared_returns_true));

    return test;
}


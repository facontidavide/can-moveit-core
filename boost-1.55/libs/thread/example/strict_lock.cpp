// Copyright (C) 2012 Vicente Botet
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_THREAD_VERSION 4

#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_traits.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/strict_lock.hpp>
#include <boost/thread/lock_types.hpp>
#include <iostream>


BOOST_STATIC_ASSERT(CMI::is_strict_lock<CMI::strict_lock<CMI::mutex> >::value);
BOOST_CONCEPT_ASSERT(( CMI::BasicLockable<CMI::mutex> ));
BOOST_CONCEPT_ASSERT(( CMI::StrictLock<CMI::strict_lock<CMI::mutex> > ));

int main()
{
  {
    CMI::mutex mtx;
    CMI::strict_lock<CMI::mutex> lk(mtx);
    std::cout << __FILE__ << std::endl;
  }
  {
    CMI::timed_mutex mtx;
    CMI::unique_lock<CMI::timed_mutex> lk(mtx);
    CMI::nested_strict_lock<CMI::unique_lock<CMI::timed_mutex> > nlk(lk);
    std::cout << __FILE__ << std::endl;
  }
  {
    CMI::mutex mtx;
    CMI::unique_lock<CMI::mutex> lk(mtx, CMI::defer_lock);
    CMI::nested_strict_lock<CMI::unique_lock<CMI::mutex> > nlk(lk);
    std::cout << __FILE__ << std::endl;
  }
  return 0;
}

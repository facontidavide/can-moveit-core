//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <ctime>
#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using CMI::asio::ip::udp;

std::string make_daytime_string()
{
  using namespace std; // For time_t, time and ctime;
  time_t now = time(0);
  return ctime(&now);
}

int main()
{
  try
  {
    CMI::asio::io_service io_service;

    udp::socket socket(io_service, udp::endpoint(udp::v4(), 13));

    for (;;)
    {
      CMI::array<char, 1> recv_buf;
      udp::endpoint remote_endpoint;
      CMI::system::error_code error;
      socket.receive_from(CMI::asio::buffer(recv_buf),
          remote_endpoint, 0, error);

      if (error && error != CMI::asio::error::message_size)
        throw CMI::system::system_error(error);

      std::string message = make_daytime_string();

      CMI::system::error_code ignored_error;
      socket.send_to(CMI::asio::buffer(message),
          remote_endpoint, 0, ignored_error);
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}

//
// timer.cpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

void print(const CMI::system::error_code& /*e*/,
    CMI::asio::deadline_timer* t, int* count)
{
  if (*count < 5)
  {
    std::cout << *count << "\n";
    ++(*count);

    t->expires_at(t->expires_at() + CMI::posix_time::seconds(1));
    t->async_wait(CMI::bind(print,
          CMI::asio::placeholders::error, t, count));
  }
}

int main()
{
  CMI::asio::io_service io;

  int count = 0;
  CMI::asio::deadline_timer t(io, CMI::posix_time::seconds(1));
  t.async_wait(CMI::bind(print,
        CMI::asio::placeholders::error, &t, &count));

  io.run();

  std::cout << "Final count is " << count << "\n";

  return 0;
}

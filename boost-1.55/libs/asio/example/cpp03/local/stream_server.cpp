//
// stream_server.cpp
// ~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdio>
#include <iostream>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#if defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

using CMI::asio::local::stream_protocol;

class session
  : public CMI::enable_shared_from_this<session>
{
public:
  session(CMI::asio::io_service& io_service)
    : socket_(io_service)
  {
  }

  stream_protocol::socket& socket()
  {
    return socket_;
  }

  void start()
  {
    socket_.async_read_some(CMI::asio::buffer(data_),
        CMI::bind(&session::handle_read,
          shared_from_this(),
          CMI::asio::placeholders::error,
          CMI::asio::placeholders::bytes_transferred));
  }

  void handle_read(const CMI::system::error_code& error,
      size_t bytes_transferred)
  {
    if (!error)
    {
      CMI::asio::async_write(socket_,
          CMI::asio::buffer(data_, bytes_transferred),
          CMI::bind(&session::handle_write,
            shared_from_this(),
            CMI::asio::placeholders::error));
    }
  }

  void handle_write(const CMI::system::error_code& error)
  {
    if (!error)
    {
      socket_.async_read_some(CMI::asio::buffer(data_),
          CMI::bind(&session::handle_read,
            shared_from_this(),
            CMI::asio::placeholders::error,
            CMI::asio::placeholders::bytes_transferred));
    }
  }

private:
  // The socket used to communicate with the client.
  stream_protocol::socket socket_;

  // Buffer used to store data received from the client.
  CMI::array<char, 1024> data_;
};

typedef CMI::shared_ptr<session> session_ptr;

class server
{
public:
  server(CMI::asio::io_service& io_service, const std::string& file)
    : io_service_(io_service),
      acceptor_(io_service, stream_protocol::endpoint(file))
  {
    session_ptr new_session(new session(io_service_));
    acceptor_.async_accept(new_session->socket(),
        CMI::bind(&server::handle_accept, this, new_session,
          CMI::asio::placeholders::error));
  }

  void handle_accept(session_ptr new_session,
      const CMI::system::error_code& error)
  {
    if (!error)
    {
      new_session->start();
    }

    new_session.reset(new session(io_service_));
    acceptor_.async_accept(new_session->socket(),
        CMI::bind(&server::handle_accept, this, new_session,
          CMI::asio::placeholders::error));
  }

private:
  CMI::asio::io_service& io_service_;
  stream_protocol::acceptor acceptor_;
};

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: stream_server <file>\n";
      std::cerr << "*** WARNING: existing file is removed ***\n";
      return 1;
    }

    CMI::asio::io_service io_service;

    std::remove(argv[1]);
    server s(io_service, argv[1]);

    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}

#else // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)
# error Local sockets not available on this platform.
#endif // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

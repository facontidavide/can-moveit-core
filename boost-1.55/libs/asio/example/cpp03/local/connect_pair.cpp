//
// connect_pair.cpp
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <iostream>
#include <string>
#include <cctype>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#if defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

using CMI::asio::local::stream_protocol;

class uppercase_filter
{
public:
  uppercase_filter(CMI::asio::io_service& io_service)
    : socket_(io_service)
  {
  }

  stream_protocol::socket& socket()
  {
    return socket_;
  }

  void start()
  {
    // Wait for request.
    socket_.async_read_some(CMI::asio::buffer(data_),
        CMI::bind(&uppercase_filter::handle_read,
          this, CMI::asio::placeholders::error,
          CMI::asio::placeholders::bytes_transferred));
  }

private:
  void handle_read(const CMI::system::error_code& ec, std::size_t size)
  {
    if (!ec)
    {
      // Compute result.
      for (std::size_t i = 0; i < size; ++i)
        data_[i] = std::toupper(data_[i]);

      // Send result.
      CMI::asio::async_write(socket_, CMI::asio::buffer(data_, size),
          CMI::bind(&uppercase_filter::handle_write,
            this, CMI::asio::placeholders::error));
    }
    else
    {
      throw CMI::system::system_error(ec);
    }
  }

  void handle_write(const CMI::system::error_code& ec)
  {
    if (!ec)
    {
      // Wait for request.
      socket_.async_read_some(CMI::asio::buffer(data_),
          CMI::bind(&uppercase_filter::handle_read,
            this, CMI::asio::placeholders::error,
            CMI::asio::placeholders::bytes_transferred));
    }
    else
    {
      throw CMI::system::system_error(ec);
    }
  }

  stream_protocol::socket socket_;
  CMI::array<char, 512> data_;
};

void run(CMI::asio::io_service* io_service)
{
  try
  {
    io_service->run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception in thread: " << e.what() << "\n";
    std::exit(1);
  }
}

int main()
{
  try
  {
    CMI::asio::io_service io_service;

    // Create filter and establish a connection to it.
    uppercase_filter filter(io_service);
    stream_protocol::socket socket(io_service);
    CMI::asio::local::connect_pair(socket, filter.socket());
    filter.start();

    // The io_service runs in a background thread to perform filtering.
    CMI::thread thread(CMI::bind(run, &io_service));

    for (;;)
    {
      // Collect request from user.
      std::cout << "Enter a string: ";
      std::string request;
      std::getline(std::cin, request);

      // Send request to filter.
      CMI::asio::write(socket, CMI::asio::buffer(request));

      // Wait for reply from filter.
      std::vector<char> reply(request.size());
      CMI::asio::read(socket, CMI::asio::buffer(reply));

      // Show reply to user.
      std::cout << "Result: ";
      std::cout.write(&reply[0], request.size());
      std::cout << std::endl;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
    std::exit(1);
  }
}

#else // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)
# error Local sockets not available on this platform.
#endif // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

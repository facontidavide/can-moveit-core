//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

typedef CMI::asio::ssl::stream<CMI::asio::ip::tcp::socket> ssl_socket;

class session
{
public:
  session(CMI::asio::io_service& io_service,
      CMI::asio::ssl::context& context)
    : socket_(io_service, context)
  {
  }

  ssl_socket::lowest_layer_type& socket()
  {
    return socket_.lowest_layer();
  }

  void start()
  {
    socket_.async_handshake(CMI::asio::ssl::stream_base::server,
        CMI::bind(&session::handle_handshake, this,
          CMI::asio::placeholders::error));
  }

  void handle_handshake(const CMI::system::error_code& error)
  {
    if (!error)
    {
      socket_.async_read_some(CMI::asio::buffer(data_, max_length),
          CMI::bind(&session::handle_read, this,
            CMI::asio::placeholders::error,
            CMI::asio::placeholders::bytes_transferred));
    }
    else
    {
      delete this;
    }
  }

  void handle_read(const CMI::system::error_code& error,
      size_t bytes_transferred)
  {
    if (!error)
    {
      CMI::asio::async_write(socket_,
          CMI::asio::buffer(data_, bytes_transferred),
          CMI::bind(&session::handle_write, this,
            CMI::asio::placeholders::error));
    }
    else
    {
      delete this;
    }
  }

  void handle_write(const CMI::system::error_code& error)
  {
    if (!error)
    {
      socket_.async_read_some(CMI::asio::buffer(data_, max_length),
          CMI::bind(&session::handle_read, this,
            CMI::asio::placeholders::error,
            CMI::asio::placeholders::bytes_transferred));
    }
    else
    {
      delete this;
    }
  }

private:
  ssl_socket socket_;
  enum { max_length = 1024 };
  char data_[max_length];
};

class server
{
public:
  server(CMI::asio::io_service& io_service, unsigned short port)
    : io_service_(io_service),
      acceptor_(io_service,
          CMI::asio::ip::tcp::endpoint(CMI::asio::ip::tcp::v4(), port)),
      context_(CMI::asio::ssl::context::sslv23)
  {
    context_.set_options(
        CMI::asio::ssl::context::default_workarounds
        | CMI::asio::ssl::context::no_sslv2
        | CMI::asio::ssl::context::single_dh_use);
    context_.set_password_callback(CMI::bind(&server::get_password, this));
    context_.use_certificate_chain_file("server.pem");
    context_.use_private_key_file("server.pem", CMI::asio::ssl::context::pem);
    context_.use_tmp_dh_file("dh512.pem");

    start_accept();
  }

  std::string get_password() const
  {
    return "test";
  }

  void start_accept()
  {
    session* new_session = new session(io_service_, context_);
    acceptor_.async_accept(new_session->socket(),
        CMI::bind(&server::handle_accept, this, new_session,
          CMI::asio::placeholders::error));
  }

  void handle_accept(session* new_session,
      const CMI::system::error_code& error)
  {
    if (!error)
    {
      new_session->start();
    }
    else
    {
      delete new_session;
    }

    start_accept();
  }

private:
  CMI::asio::io_service& io_service_;
  CMI::asio::ip::tcp::acceptor acceptor_;
  CMI::asio::ssl::context context_;
};

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: server <port>\n";
      return 1;
    }

    CMI::asio::io_service io_service;

    using namespace std; // For atoi.
    server s(io_service, atoi(argv[1]));

    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}

//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//  Adaptation to Boost of the libcxx
//  Copyright 2010 Vicente J. Botet Escriba
//  Distributed under the Boost Software License, Version 1.0.
//  See http://www.boost.org/LICENSE_1_0.txt


#include <boost/chrono/duration.hpp>
#include <boost/type_traits.hpp>
#include <limits>

#if !defined(BOOST_NO_CXX11_STATIC_ASSERT)
#define NOTHING ""
#endif

template <typename D, int ExpectedDigits, typename ExpectedPeriod>
void check_duration()
{
    typedef typename D::rep Rep;
    typedef typename D::period Period;
    BOOST_CHRONO_STATIC_ASSERT(CMI::is_signed<Rep>::value, NOTHING, ());
    BOOST_CHRONO_STATIC_ASSERT(CMI::is_integral<Rep>::value, NOTHING, ());
    BOOST_CHRONO_STATIC_ASSERT(std::numeric_limits<Rep>::digits >= ExpectedDigits, NOTHING, ());
    BOOST_CHRONO_STATIC_ASSERT((CMI::is_same<Period, ExpectedPeriod >::value), NOTHING, ());
}

void test()
{
    check_duration<CMI::chrono::hours, 22, CMI::ratio<3600> >();
    check_duration<CMI::chrono::minutes, 28, CMI::ratio<60> >();
    check_duration<CMI::chrono::seconds, 34, CMI::ratio<1> >();
    check_duration<CMI::chrono::milliseconds, 44, CMI::milli >();
    check_duration<CMI::chrono::microseconds, 54, CMI::micro >();
    check_duration<CMI::chrono::nanoseconds, 63, CMI::nano >();
}

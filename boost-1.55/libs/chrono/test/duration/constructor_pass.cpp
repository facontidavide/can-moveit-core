//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//  Adaptation to Boost of the libcxx
//  Copyright 2010 Vicente J. Botet Escriba
//  Distributed under the Boost Software License, Version 1.0.
//  See http://www.boost.org/LICENSE_1_0.txt


#include <boost/chrono/duration.hpp>
#include <boost/detail/lightweight_test.hpp>


#include "../rep.h"
#include <iostream>

#ifdef BOOST_NO_CXX11_CONSTEXPR
#define BOOST_CONSTEXPR_ASSERT(C) BOOST_TEST(C)
#else
#include <boost/static_assert.hpp>
#define BOOST_CONSTEXPR_ASSERT(C) BOOST_STATIC_ASSERT(C)
#endif

template <class D>
void
check_default()
{
  {
    D d;
    BOOST_TEST(d.count() == typename D::rep());
  }
  {
    BOOST_CONSTEXPR D d;
    BOOST_CONSTEXPR_ASSERT(d.count() == typename D::rep());
  }
}

template <class D, class R>
void
check_from_rep(R r)
{
  {
    D d(r);
    BOOST_TEST(d.count() == r);
  }
}

int main()
{
    // exact conversions allowed for integral reps
    {
        CMI::chrono::milliseconds ms(1);
        CMI::chrono::microseconds us = ms;
        BOOST_TEST(us.count() == 1000);
        {
          BOOST_CONSTEXPR CMI::chrono::milliseconds ms(1);
          BOOST_CONSTEXPR CMI::chrono::microseconds us = ms;
          BOOST_CONSTEXPR_ASSERT(us.count() == 1000);
        }
    }
    // inexact conversions allowed for floating point reps
    {
        CMI::chrono::duration<double, CMI::micro> us(1);
        CMI::chrono::duration<double, CMI::milli> ms = us;
        BOOST_TEST(ms.count() == 1./1000);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<double, CMI::micro> us(1);
          BOOST_CONSTEXPR CMI::chrono::duration<double, CMI::milli> ms = us;
          BOOST_CONSTEXPR_ASSERT(ms.count() == 1./1000);
        }
    }
    // Convert int to float
    {
        CMI::chrono::duration<int> i(3);
        CMI::chrono::duration<double> d = i;
        BOOST_TEST(d.count() == 3);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<int> i(3);
          BOOST_CONSTEXPR CMI::chrono::duration<double> d = i;
          BOOST_CONSTEXPR_ASSERT(d.count() == 3);
        }
    }
    // default constructor
    {
      check_default<CMI::chrono::duration<Rep> >();
    }
    // constructor from rep
    {
        check_from_rep<CMI::chrono::duration<int> >(5);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<int> d(5);
          BOOST_CONSTEXPR_ASSERT(d.count() == 5);
        }
        check_from_rep<CMI::chrono::duration<int, CMI::ratio<3, 2> > >(5);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<int, CMI::ratio<3, 2> > d(5);
          BOOST_CONSTEXPR_ASSERT(d.count() == 5);
        }
        check_from_rep<CMI::chrono::duration<Rep, CMI::ratio<3, 2> > >(Rep(3));
        {
          BOOST_CONSTEXPR CMI::chrono::duration<Rep, CMI::ratio<3, 2> > d(Rep(3));
          BOOST_CONSTEXPR_ASSERT(d.count() == Rep(3));
        }
        check_from_rep<CMI::chrono::duration<double, CMI::ratio<2, 3> > >(5.5);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<double, CMI::ratio<3, 2> > d(5.5);
          BOOST_CONSTEXPR_ASSERT(d.count() == 5.5);
        }


    }
    // constructor from other rep
    {
        CMI::chrono::duration<double> d(5);
        BOOST_TEST(d.count() == 5);
        {
          BOOST_CONSTEXPR CMI::chrono::duration<double> d(5);
          BOOST_CONSTEXPR_ASSERT(d.count() == 5);
        }
    }

    return CMI::report_errors();
}

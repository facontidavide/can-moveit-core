//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//  Adaptation to Boost of the libcxx
//  Copyright 2010 Vicente J. Botet Escriba
//  Distributed under the Boost Software License, Version 1.0.
//  See http://www.boost.org/LICENSE_1_0.txt

#include <boost/chrono/duration.hpp>
#include <boost/type_traits.hpp>
#include <boost/detail/lightweight_test.hpp>
#if !defined(BOOST_NO_CXX11_STATIC_ASSERT)
#define NOTHING ""
#endif

#ifdef BOOST_NO_CXX11_CONSTEXPR
#define BOOST_CONSTEXPR_ASSERT(C) BOOST_TEST(C)
#else
#include <boost/static_assert.hpp>
#define BOOST_CONSTEXPR_ASSERT(C) BOOST_STATIC_ASSERT(C)
#endif


template <class ToDuration, class FromDuration>
void
test(const FromDuration& f, const ToDuration& d)
{
//~ #if defined(BOOST_NO_CXX11_DECLTYPE)
    //~ typedef BOOST_TYPEOF_TPL(CMI::chrono::duration_cast<ToDuration>(f)) R;
//~ #else
    //~ typedef decltype(CMI::chrono::duration_cast<ToDuration>(f)) R;
//~ #endif
    //~ BOOST_CHRONO_STATIC_ASSERT((CMI::is_same<R, ToDuration>::value), NOTHING, (R, ToDuration));
    BOOST_TEST(CMI::chrono::duration_cast<ToDuration>(f) == d);
}

int main()
{
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::hours(2));
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::minutes(121));
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::seconds(7265));
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::milliseconds(7265000));
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::microseconds(7265000000LL));
    test(CMI::chrono::milliseconds(7265000), CMI::chrono::nanoseconds(7265000000000LL));
    test(CMI::chrono::milliseconds(7265000),
         CMI::chrono::duration<double, CMI::ratio<3600> >(7265./3600));
    test(CMI::chrono::duration<int, CMI::ratio<2, 3> >(9),
         CMI::chrono::duration<int, CMI::ratio<3, 5> >(10));
    {
      BOOST_CONSTEXPR CMI::chrono::hours h = CMI::chrono::duration_cast<CMI::chrono::hours>(CMI::chrono::milliseconds(7265000));
      BOOST_CONSTEXPR_ASSERT(h.count() == 2);
    }
    return CMI::report_errors();
}

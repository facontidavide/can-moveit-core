#ifndef BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED
#define BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED

// MS compatible compilers support #pragma once

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

//
//  bind/placeholders.hpp - _N definitions
//
//  Copyright (c) 2002 Peter Dimov and Multi Media Ltd.
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//
//  See http://www.boost.org/libs/bind/bind.html for documentation.
//

#include <boost/bind/arg.hpp>
#include <boost/config.hpp>

namespace
{

#if defined(__BORLANDC__) || defined(__GNUC__) && (__GNUC__ < 4)

static inline CMI::arg<1> _1() { return CMI::arg<1>(); }
static inline CMI::arg<2> _2() { return CMI::arg<2>(); }
static inline CMI::arg<3> _3() { return CMI::arg<3>(); }
static inline CMI::arg<4> _4() { return CMI::arg<4>(); }
static inline CMI::arg<5> _5() { return CMI::arg<5>(); }
static inline CMI::arg<6> _6() { return CMI::arg<6>(); }
static inline CMI::arg<7> _7() { return CMI::arg<7>(); }
static inline CMI::arg<8> _8() { return CMI::arg<8>(); }
static inline CMI::arg<9> _9() { return CMI::arg<9>(); }

#elif defined(BOOST_MSVC) || (defined(__DECCXX_VER) && __DECCXX_VER <= 60590031) || defined(__MWERKS__) || \
    defined(__GNUC__) && (__GNUC__ == 4 && __GNUC_MINOR__ < 2)  

static CMI::arg<1> _1;
static CMI::arg<2> _2;
static CMI::arg<3> _3;
static CMI::arg<4> _4;
static CMI::arg<5> _5;
static CMI::arg<6> _6;
static CMI::arg<7> _7;
static CMI::arg<8> _8;
static CMI::arg<9> _9;

#else

CMI::arg<1> _1;
CMI::arg<2> _2;
CMI::arg<3> _3;
CMI::arg<4> _4;
CMI::arg<5> _5;
CMI::arg<6> _6;
CMI::arg<7> _7;
CMI::arg<8> _8;
CMI::arg<9> _9;

#endif

} // unnamed namespace

#endif // #ifndef BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED
